// config/config.default.js
exports.keys = "kljsfss9089234hkasdfu@(880312";
exports.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.nj': 'nunjucks',
    },
    
};
// 修改模块的默认标签{{ }}
exports.nunjucks = {
    autoescape: false,
    tags: {
        blockStart: '<%',
        blockEnd: '%>',
        variableStart: '<$',
        variableEnd: '$>',
        commentStart: '<#',
        commentEnd: '#>'
    }
}

exports.mysql = {
    //database configuration 
    client:{
        //host 
        host:'localhost',
        //port 
        port:'3306',
        //username 
        user:'root',
        //password 
        password:'19800116',
        //database 
        database:'buysys'
    },
    //load into app,default is open //加载到应用程序，默认为打开
    app:true,
    //load into agent,default is close //加载到代理中，默认值为“关闭”
    agent:false,
};