exports.nunjucks = {
    enable: true,
    package: 'egg-view-nunjucks',
};

exports.mysql = {
  //mysql
  enable: true,
  package: 'egg-mysql',
};