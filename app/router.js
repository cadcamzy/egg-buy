// app/router.js
module.exports = app => {
    const { router, controller } = app;
    router.get('/', controller.home.index);
    router.get('/view/console/index', controller.view.console.index)
    router.get('/basicSys/item_info', controller.basicSys.iteminfo.index)
    router.get('/basicSys/item_info/list', controller.basicSys.iteminfo.list)
    router.get('/basicSys/unit_info', controller.basicSys.unitinfo.index)
    router.get('/basicSys/unit_info/list', controller.basicSys.unitinfo.list)
    router.get('/basicSys/unit_info/:id', controller.basicSys.unitinfo.get)
    router.post('/basicSys/unit_info/insert', controller.basicSys.unitinfo.insert)
    router.post('/basicSys/unit_info/update', controller.basicSys.unitinfo.update)
    router.post('/basicSys/unit_info/delete', controller.basicSys.unitinfo.delete)


};