'use strict';

const Service = require('egg').Service;

class UnitInfo extends Service {
    async get(id) {
      const items = await this.app.mysql.get('unit_info', { id });
      return items;
    }
  
    async list(pageSize = 20, pageNo = 1, orderBy = 'id', order = 'ASC') {
  
      let sql = 'select id, name from unit_info ';
      sql += 'order by ? ? ';
      sql += 'limit ?,?;';

      const offset = pageSize * (pageNo - 1);
      try {
        const result = await this.app.mysql.query(sql, [ orderBy, order, offset, pageSize ]);
        return result
      } catch (error) {
          return error
      }
    }
  
    async insert(item) {
      const result = await this.app.mysql.insert('unit_info', {name: item.name});
      return result;
    }
  
    async update(user) {
      const result = await this.app.mysql.update('unit_info', user);
      return result;
    }
  
    // 软删除
    async delete(id) {
      const result = await this.app.mysql.delete('unit_info', { id });
      return result;
    }
  }
  
  module.exports = UnitInfo;