'use strict';

const Service = require('egg').Service;

class ItemInfo extends Service {
    async get(id) {
      const items = await this.app.mysql.get('item_info', { id });
      return items;
    }
  
    async list(pageSize = 20, pageNo = 1, orderBy = 'id', order = 'ASC') {
  
      let sql = 'select id, code, name, module, unit, remark from item_info ';
      sql += 'order by ? ? ';
      sql += 'limit ?,?;';
  
      const offset = pageSize * (pageNo - 1);
      return await this.app.mysql.query(sql, [ orderBy, order, offset, pageSize ]);
    }
  
    async insert(item) {
      const result = await this.app.mysql.insert('item_info', item);
      return result;
    }
  
    async update(user) {
      const result = await this.app.mysql.update('item_info', user);
      return result;
    }
  
    // 软删除
    async delete(id) {
      const result = await this.app.mysql.update('item_info', {
        id,
        deleted: 1,
      });
      return result;
    }
  
    // 从表结构中移除
    async hardDelete(id) {
      const result = await this.app.mysql.delete('item_info', { id });
      return result;
    }
  }
  
  module.exports = ItemInfo;