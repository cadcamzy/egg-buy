// app/controller/home.js
const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
      const ctx = this.ctx;
      ctx.body = await ctx.renderView('index.html', "data");
  }
}

module.exports = HomeController;