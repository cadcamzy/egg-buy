
const Controller = require('egg').Controller;

class UnitInfoController extends Controller {
  async index() {
      const ctx = this.ctx;
      ctx.body = await ctx.renderView('/basicSys/unit_info.html', "data");
  }

  async get() {
    // unit_info/:id
    const ctx = this.ctx;
    const id = ctx.params.id;

    const result = await this.service.basicSys.unitinfo.get(id);

    ctx.body = result;
  }

  async list() {
    const ctx = this.ctx;
    const result = await this.service.basicSys.unitinfo.list(20, 1, 'id', 'ASC')

    ctx.body = {
      "code": 0,
      "msg": "...",
      "count": result ? result.count : 0,
      "data": result || []
    }
  }

  async insert() {
    const ctx = this.ctx;
    const item = this.ctx.request.body;
    
    const result = await this.service.basicSys.unitinfo.insert(item);
    ctx.body = {
      success: result.affectedRows && result.affectedRows > 0,
      msg: result.affectedRows ? '新增成功！' : '新增失败！'
    }

  }

  async update() {
    const ctx = this.ctx;
    const item = this.ctx.request.body;

    const result = await this.service.basicSys.unitinfo.update(item);
    ctx.body = {
      success: result.affectedRows && result.affectedRows > 0,
      msg: result.affectedRows ? '更新成功！' : '更新失败！'
    }
  }

  async delete() {
    const ctx = this.ctx;
    const item = this.ctx.request.body;

    const result = await this.service.basicSys.unitinfo.delete(item);
    ctx.body = {
      success: result.affectedRows && result.affectedRows > 0,
      msg: result.affectedRows ? '删除成功！' : '删除失败！'
    }
  }
}

module.exports = UnitInfoController;