
const Controller = require('egg').Controller;

class ItemInfoController extends Controller {
  async index() {
      const ctx = this.ctx;
      ctx.body = await ctx.renderView('/basicSys/item_info.html', "data");
  }
  async list() {
    const ctx = this.ctx;
    const result = await this.service.basicSys.iteminfo.list

    ctx.body = {
      "code": 0,
      "msg": "...",
      "count": result.count,
      "data": result
    }
  }

  async insert() {
    const ctx = this.ctx;
    const item = this.ctx.request.body;
    delete item.id;
    
    const result = await this.service.basicSys.iteminfo.insert(item);

    ctx.body = {

    }

  }
}

module.exports = ItemInfoController;