
const Controller = require('egg').Controller;

class ConsoleController extends Controller {
  async index() {
      const ctx = this.ctx;
      ctx.body = await ctx.renderView('/view/console/console1.html', "data");
  }
}

module.exports = ConsoleController;